﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.Tests
{
    [TestFixture]
    public class ConditionalTests
    {
        [TestCase(true,true,true)]
        [TestCase(false,false,true)]
        [TestCase(true,false, false)]
        public void AreWeInTroubleTest(bool aSmile, bool bSmile, bool expected)
        {
            Conditionals target = new Conditionals();
            bool actual = target.AreWeInTrouble(aSmile, bSmile);
            Assert.AreEqual(expected, actual);
        }

    }
}
