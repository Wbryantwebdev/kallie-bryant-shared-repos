﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Test
{
    [TestFixture]
    public class StringWarmsTest
    {
        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void Exercise1Test(string input, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.SayHi(input);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void Exercise2Test(string input1, string input2, string expected)
        {
            //Arrange figure out the structure of the test
            StringWarms target = new StringWarms();

            //Act
            string actual = target.Abba(input1, input2);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]
        public void Exercise3Test(string tag, string content, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.MakeTags(tag, content);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "word", "[[word]]")]
        public void Exercise4Test(string container, string word, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.InsertWord(container, word);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]

        public void Exercise5test(string str, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.MultipleEndings(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }
        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]

        public void Exercise6test(string str, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.FirstHalf(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
