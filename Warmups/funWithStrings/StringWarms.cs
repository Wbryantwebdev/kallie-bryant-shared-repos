﻿namespace Warmups
{
    public class StringWarms
    {
        public string SayHi(string input)
        {
            return "Hello " + input + "!";
        }
        public string Abba(string a, string b)
        {
            string result = a + b + b + a;

            return result;
        }
        public string MakeTags(string tag, string content)
        {

            string result = $"<{tag}>{content}</{tag}>";

            return result;
        }
        public string InsertWord(string container, string word)
        {

            string result = container.Substring(0, 2) + word + container.Substring(2, 2);

            return result;
        }
        public string MultipleEndings(string str)
            {
            string result = str.Substring(str.Length - 2);

            return result + result + result;
            }
        public string FirstHalf(string str)
        {
            string result = str.Substring(0, str.Length / 2);

            return result;
        }
    }

}