﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class Conditionals
    {
        /// <summary>
        /// We have two children, a and b, and the
        /// parameters aSmile and bSmile indicate 
        /// if each is smiling. We are in trouble 
        /// if they are both smiling or if neither 
        /// of them is smiling. Return true if we 
        /// are in trouble. 
        /// </summary>
        /// <param name="aSmile"></param>
        /// <param name="bSmile"></param>
        /// <returns></returns>
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if( aSmile == bSmile)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CanSleepIn(bool isWeekday, bool isVacation)
        {
            throw new NotImplementedException();

        }

    }
}
