﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace funWithStrings.Test
{
    [TestFixture]
    public class StringWarmsTest
    {
        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]

        public void Exercise1Test(string input, string expected)
        {
            //Arrange- this tells the computer where to find the code you want to test in this case it's in the StringWarms class
            StringWarms target = new StringWarms();

            //Act- What action do I want to perform? This creates a variable named actual which is of type string and sets it equal to inputs string value which is the value in the SayHi method
            string actual = target.SayHi(input);

            //Assert- short for assertain, the .AreEqual property checks the expected result against the actual result to see if they are equal or True
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("Hi", "Bye", "HiByeByeHi")]

        public void Exercise2Test(string input1, string input2, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.Abba(input1, input2);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]

        public void Excercise3Test(string input1, string input2, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.MakeTags(input1, input2);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]

        public void Excercise4Test(string container, string word, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.MakeTags(container, word);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void Excercise5Test(string str, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.MultipleEndings(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void Excercise6Test(string str, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.FirstHalf(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "ell")]
        [TestCase("java", "av")]
        [TestCase("coding", "odin")]
        public void Excercise7Test(string str, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.TrimOne(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]
        public void Excercise8Test(string a, string b, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.LongInMiddle(a, b);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void Excercise9Test(string str, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.RotateLeft2(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "loHel")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void Excercise10Test(string str, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.RotateRight2(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false, "o")]
        [TestCase("oh", true, "o")]
        public void Excercise11Test(string str, bool fromFront, string expected)
        {
            //Arrange
            StringWarms target = new StringWarms();

            //Act
            string actual = target.TakeOne(str, fromFront);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("string", "ri")]
        [TestCase("code", "od")]
        [TestCase("practice", "ct")]
        public void Excercise12Test(string str, string expected)
        {
            StringWarms target = new StringWarms();

            string actual = target.MiddleTwo(str);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("oddly", true)]
        [TestCase("y", false)]
        [TestCase("oddy", false)]
        public void Excercise13Test(string str, bool expected)
        {
            StringWarms target = new StringWarms();

            bool actual = target.EndsWithLy(str);

            Assert.AreEqual(expected, actual);
        }
         
        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void Excercise14Test(string str, int n, string expected)
        {
            StringWarms target = new StringWarms();

            string actual = target.FrontAndBack(str,n);

            Assert.AreEqual(expected, actual);
        }
    }
}
