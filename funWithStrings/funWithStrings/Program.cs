﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace funWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            
            StringWarms prog = new StringWarms();

            //Console.WriteLine(prog.SayHi("Bob"));
            //Console.WriteLine(prog.SayHi("Alice"));
            //Console.WriteLine(prog.SayHi("X"));

            //Console.WriteLine(prog.Abba("Hi", "Bye"));
            //Console.WriteLine(prog.Abba("Yo", "Alice"));
            //Console.WriteLine(prog.Abba("What", "Up"));

            //Console.WriteLine(prog.MakeTags("i", "Yay"));
            //Console.WriteLine(prog.MakeTags("i", "Hello"));
            //Console.WriteLine(prog.MakeTags("cite", "Yay"));

            //Console.WriteLine(prog.InsertWord("<<>>", "Yay"));
            //Console.WriteLine(prog.InsertWord("<<>>", "WooHoo"));
            //Console.WriteLine(prog.InsertWord("[[]]", "Word"));

            //Console.WriteLine(prog.MultipleEndings("Hello"));
            //Console.WriteLine(prog.MultipleEndings("ab"));
            //Console.WriteLine(prog.MultipleEndings("Hi"));

            //Console.WriteLine(prog.FirstHalf("WooHoo"));
            //Console.WriteLine(prog.FirstHalf("HelloThere"));
            //Console.WriteLine(prog.FirstHalf("abcdef"));

            //**Console.WriteLine(prog.TrimOne("Hello"));
            //Console.WriteLine(prog.TrimOne("java"));
            //Console.WriteLine(prog.TrimOne("coding"));

            //Console.WriteLine(prog.LongInMiddle("Hello", "hi"));
            //Console.WriteLine(prog.LongInMiddle("hi", "Hello"));
            //Console.WriteLine(prog.LongInMiddle("aaa", "b"));

            //Console.WriteLine(prog.RotateLeft2("Hello"));
            //Console.WriteLine(prog.RotateLeft2("java"));
            //Console.WriteLine(prog.RotateLeft2("Hi"));

            //Console.WriteLine(prog.RotateRight2("Hello"));
            //Console.WriteLine(prog.RotateRight2("java"));
            //Console.WriteLine(prog.RotateRight2("Hi"));

            //Console.WriteLine(prog.TakeOne("Hello", true));
            //Console.WriteLine(prog.TakeOne("Hello", false));
            //Console.WriteLine(prog.TakeOne("oh", true));

            //Console.WriteLine(prog.MiddleTwo("string"));
            //Console.WriteLine(prog.MiddleTwo("code"));
            //Console.WriteLine(prog.MiddleTwo("practice"));

            //Console.WriteLine(prog.EndsWithLy("oddly"));
            //Console.WriteLine(prog.EndsWithLy("y"));
            //Console.WriteLine(prog.EndsWithLy("oddy"));

            Console.WriteLine(prog.FrontAndBack("Hello", 2));
            Console.WriteLine(prog.FrontAndBack("Chocolate", 3));
            Console.WriteLine(prog.FrontAndBack("Chocolate", 1));

            Console.ReadLine();
        }

    }
}