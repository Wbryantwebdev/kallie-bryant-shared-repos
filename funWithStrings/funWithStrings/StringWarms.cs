﻿using System;
using System.Xml.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace funWithStrings
{ 
    public class StringWarms
    {
        public string SayHi(string input)
        {
            return "Hello " + input + "!";
        }


        public string Abba(string a, string b)
        {
            return a + b + b + a;
        }

        public string MakeTags(string a, string b)
        {
            return $"<{a}>{b}</{a}>";
        }

        public string InsertWord(string container, string word)
        {
            string result = container.Substring(0, 2) + word + container.Substring(2, 2);
           
            return result;
        }

        public string MultipleEndings(string str)
        {
            string result = str.Substring(str.Length-2);
            return result + result + result;
        }

        public string FirstHalf(string str)
        {
            return str.Substring(0, str.Length/2);
        }

        public string TrimOne(string str)
        {
            return str.Substring(1, str.Length-2);
        }

        public string LongInMiddle(string a, string b)
        {
            if (a.Length > b.Length)
            {
                return b + a + b;
            }
            else
            {
                return a + b + a;
            }
        }

        public string RotateLeft2(string str)
        {
            if (str.Length > 2)
            {
               string result = str.Substring(2, str.Length-2) + (str.Substring(0,2));
                return result;
            }
            else 
            {
                string result = str;
                return result;
            }
        }

        public string RotateRight2(string str)
        {
            return str.Substring(str.Length - 2) + str.Substring(0, str.Length - 2);
        }

        public string TakeOne(string str, bool fromFront)
        {
            if (fromFront == true)
            {
                return str.Substring(0, 1);
            }
            else
            {
                return str.Substring(str.Length - 1);
            }
        }

        public string MiddleTwo(string str)
        {
            int half = str.Length/2;
            return str.Substring(half - 1, 2);
        }

        public bool EndsWithLy(string str)
        {
            if (str.Length > 1)
            {
                string end = str.Substring(str.Length -2, 2);
                if (end == "ly")
                {
                    return true;
                }
                else
                {
                    return false;
                }
       
            }
            else
            {
                return false;
            }

          }

        //***
        public string FrontAndBack(string str, int n)
        {
            return str.Substring(0, n) + str.Substring(str.Length - n);

        }

    }
}