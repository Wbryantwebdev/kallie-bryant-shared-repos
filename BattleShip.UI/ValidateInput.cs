﻿namespace BattleShip.UI
{
    public class ValidateInput
    {
        public bool ValidateCoord(string input)
        {
            input = input.ToUpper();
            char letter;
            int number; 
            bool isValid = char.TryParse(input.Substring(0,1), out letter);
            isValid = int.TryParse(input.Substring(1), out number);
            if ((number < 0 || number > 10) || (letter < 64 || letter > 74))
            {
                isValid = false;
            }
            return (isValid);
        }
        public bool ValidateDirection(string input)
        {
            bool isValid;
            input = input.ToUpper();
            switch(input)
            {
                case "U":
                case "UP":
                case "D":
                case "DOWN":
                case "L":
                case "LEFT":
                case "R":
                case "RIGHT":
                    isValid = true;
                    break;
                default:
                    isValid = false;
                    break;
            }
                

            return isValid; 
        }
    }
}
 