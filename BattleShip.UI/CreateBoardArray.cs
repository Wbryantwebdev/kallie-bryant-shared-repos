﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    class CreateBoardArray
    {
        public char[,] InitalizeArray()
        {
            char[,] board = new char[10,10];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    board[i, j] = ' ';
                }
            }
            return board;
        }


    }
}
