﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

namespace BattleShip.UI
{
    public class SetGrid
    {
        public void DisplayGrid()
        {
            char[,] gameBoard = new char[10, 10];


            string[] letters = new string[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

            Console.WriteLine("    A   B   C   D   E   F   G   H   I   J  ");

            Console.WriteLine("  ╔═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╦═══╗");

            for (int i = 0; i < 10; i++)
            {
                Console.Write("{0} ", letters[i]);

                for (int j = 0; j < 10; j++)
                {
                    gameBoard[i, j] = '1';

                    if (j < 9)
                    {
                        Console.Write("║ " + gameBoard[i, j] + " ");
                    }
                    else
                    {

                        Console.Write("║ " + gameBoard[i, j] + " ║");
                    }
                }

                if (i == 9)
                {
                   Console.WriteLine("\n  ╚═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╩═══╝");
                }
                else
                {
                    Console.WriteLine("\n  ╠═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╬═══╣");
                }
            }

            Console.WriteLine();

            Console.ReadLine();

        }
    }
}
