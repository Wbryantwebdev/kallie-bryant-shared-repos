﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    public class MainMenu
    {
        public string GetName(string p)
        {
            Console.Write("Player {0}, enter your name to begin: ", p);
            return Console.ReadLine();
           
        }

        public string GetCoords()
        {
            Console.Write("Enter your coordinates: ");
            string coordinates = Console.ReadLine();
            return coordinates;
        }

        public string GetDirection()
        {
            Console.Write("Enter your ship direction U,D,L,R: ");
            string direction = Console.ReadLine();
            return direction.Substring(0,1).ToUpper();
        }


       

       
           

        
    }
}
