﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    
    public class TranslateInput
    {
        public int[] translate(string input)
        {
            int[] coords = new int[2];
            input = input.ToUpper();
            coords[0] = (char.Parse(input.Substring(0, 1)) - 65);// -65 is to set the input to set A to 0
            coords[1] = (int.Parse(input.Substring(1)) -1);//-1 is to set an input of 1 to 0
            return coords;
        }
    }
}
