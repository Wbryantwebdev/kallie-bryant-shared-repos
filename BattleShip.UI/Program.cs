﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI
{
   public class Program
    {
        static void Main(string[] args)
        {
            MainMenu menu = new MainMenu();
            SetGrid grid = new SetGrid();

            Player player1 = new Player();
            player1.Name = menu.GetName("1");

            Player player2 = new Player();
            player2.Name = menu.GetName("2");

            // Setting up player1 ships
            for (int i = 0; i < 5; i++)
            {
                do
                {
                    // Tell the user which ship to set up...
                    Console.WriteLine("{0} place your {1}", player1.Name, Enum.GetName(typeof (ShipType), i));

                    //Ask the user for the coords
                    Console.WriteLine("Please enter the coordinates");
                    string coords = menu.GetCoords();
                    //Ask the user for the direction
                    string direction = menu.GetDirection();
                    //Create the request
                    PlaceShipRequest request = new PlaceShipRequest()
                    {
                        Coordinate = GetCoordinate(coords),
                        Direction = GetShipDirection(direction),
                        ShipType = (ShipType)i
                    };

                    var response = player1.Board.PlaceShip(request);

                    if (response == ShipPlacement.NotEnoughSpace)
                        Console.WriteLine("There wasn't enough space please try again");
                    else if (response == ShipPlacement.Overlap)
                        Console.WriteLine("This ship overlapped another, please try again.");
                    else
                        break;
                } while (true);
                Console.ReadLine();
            }

            

            

            //grid.DisplayGrid();
        }

        private static Coordinate GetCoordinate(string str)
        {
            string xVal = str.Substring(0, 1);
            string yVal = str.Substring(1);

            int yCoord = int.Parse(yVal);
            int xCoord = GetXValFromLetter(xVal);

            return new Coordinate(yCoord, xCoord);
        }

       private static int GetXValFromLetter(string xVal)
       {
           switch (xVal)
           {
               case "A":
                   return 1;
               case "B":
                   return 2;
               case "C":
                   return 3;
               case "D":
                   return 4;
               case "E":
                   return 5;
               case "F":
                   return 6;
               case "G":
                   return 7;
               case "H":
                   return 8;
               case "I":
                   return 9;    
               default:
                   return 10;
           }
       }

       private static ShipDirection GetShipDirection(string dir)
       {
           switch (dir)
           {
               case "U":
                   return ShipDirection.Up;
               case "D":
                   return ShipDirection.Down;
               case "L":
                   return ShipDirection.Left;
               default:
                   return ShipDirection.Right;
           }
       }

      
    }
}
